package battleships

import scala.collection.immutable.TreeMap
import scala.util.Random

object Game {
  def main(args: Array[String]): Unit = {
    val shipSizes = List(2, 2, 2, 3, 3, 4)
    val boardEither = Board.generateShips(6, 6, shipSizes, 2)
    boardEither match {
      case Left(Some(board)) => {
        val boardWithDamagedShip = board.shoot(0, 0).shoot(1, 0).shoot(2, 0) // one ship has one healthy cell
        val boardWithDestroyedShip = boardWithDamagedShip.shoot(3, 0) // one ship was destroyed
      }
    }
  }

  case class Cell(x: Int, y: Int)
  object Cell {
    implicit val ordering: Ordering[Cell] = Ordering.by((cell: Cell) => (cell.y, cell.x))
  }

  case class ShipPosition(size: Int, horizontal: Boolean, startCell: Cell) {
    def getOccupiedCells: List[Cell] = {
      val variantCoord = if (horizontal) startCell.x else startCell.y

      List
      .range(variantCoord, variantCoord + size)
      .map(variant =>
        if (horizontal) Cell(variant, startCell.y) else Cell(startCell.x, variant)
      )
    }
  }

  class Ship private (
    val position: ShipPosition,
    val healthyCells: List[Cell],
    val damagedCells: List[Cell]
  ) {
    // if ship has such healthy cell, returns a new ship with that cell hit
    def getHit(cell: Cell): Option[Ship] =
      if (healthyCells.contains(cell))
        Some(new Ship(position, healthyCells.filterNot(_ == cell), cell +: damagedCells))
      else None

    def destroyed: Boolean = healthyCells.isEmpty
    def damaged: Boolean = damagedCells.nonEmpty
  }
  object Ship {
    def getIfPlaceable(shipPos: ShipPosition, board: Board): Option[Ship] = {
      if (board.gameState == GameStarted) return None

      val cellsToOccupy = shipPos.getOccupiedCells
      val unavailableCells =
        (board.healthyShips ++ board.damagedShips ++ board.destroyedShips)
          .flatMap(ship => ship.healthyCells ++ ship.damagedCells)

      val allCellsAreFree = !unavailableCells.exists(cell => cellsToOccupy.contains(cell))

      if (allCellsAreFree) Some(new Ship(shipPos, cellsToOccupy, List.empty))
      else None
    }
  }

  sealed trait GameState
  case object GameNotStarted extends GameState
  case object GameStarted extends GameState
  case object GameFinished extends GameState

  type ShipWasPlaced = Boolean
  class Board private (
    val numOfRows: Int,
    val numOfCol: Int,
    val healthyShips: List[Ship],
    val damagedShips: List[Ship],
    val destroyedShips: List[Ship],
    val missedCells: List[Cell],
    val gameState: GameState
  ) {
    def placeShip(shipPos: ShipPosition): (Board, ShipWasPlaced) =
      Ship.getIfPlaceable(shipPos, this)
        .fold((this, false))(newShip => {
          val boardWithNewShip = new Board(
            numOfRows = this.numOfRows,
            numOfCol = this.numOfCol,
            newShip +: healthyShips,
            damagedShips,
            destroyedShips,
            missedCells,
            gameState = GameNotStarted
          )
          (boardWithNewShip, true)
        })

    def shoot(row: Int, column: Int): Board = Board.shoot(this, row, column)

    def containsCell(cell: Cell): Boolean =
      cell.y < numOfRows &&
      cell.x < numOfCol &&
      cell.y >= 0 &&
      cell.x >= 0
  }

  object Board {
    type ErrorMsg = String
    type Occupied = Boolean

    def empty(rows: Int, columns: Int) = new Board(
      numOfRows = rows, numOfCol = columns,
      List.empty, List.empty, List.empty, List.empty, gameState = GameNotStarted
    )

    // returns if ship can be placed in provided position
    def canBePlaced(pos: ShipPosition, boardCells: TreeMap[Cell, Occupied]): Boolean = {
      val (variantCoord, constantCoord) =
        if (pos.horizontal) (pos.startCell.x, pos.startCell.y)
        else (pos.startCell.y, pos.startCell.x)

      val shipCells = pos.getOccupiedCells

      lazy val cellsAroundShip = List
        .range(variantCoord - 1, variantCoord + pos.size + 1)
        .flatMap(variant => List.range(constantCoord - 1, constantCoord + 2).map(const => {
          if (pos.horizontal) Cell(variant, const) else Cell(const, variant)
        }))
        .filterNot(shipCells.contains)

      val shipCellsAreFree = !shipCells.exists(cell => boardCells.getOrElse(cell, true))
      lazy val cellsAroundShipAreFree =
        !cellsAroundShip.exists(cell => boardCells.get(cell).fold(false)(occupied => occupied))

      shipCellsAreFree && cellsAroundShipAreFree
    }

    // returned TreeMap represents updated board cells with this ships' occupied cells marked as such
    // returns None if no suitable position was found
    def tryGetPositionForShip(
      shipSize: Int, boardCells: TreeMap[Cell, Occupied]
    ): Option[(ShipPosition, TreeMap[Cell, Occupied])] =
      boardCells
      .filterNot(_._2)
      .keys
      .find(currentCell =>
        canBePlaced(ShipPosition(shipSize, horizontal = true, currentCell), boardCells) ||
        canBePlaced(ShipPosition(shipSize, horizontal = false, currentCell), boardCells)
      ).map(cellToPlace => {
        val horizontalPosition = ShipPosition(shipSize, horizontal = true, cellToPlace)
        lazy val verticalPosition = ShipPosition(shipSize, horizontal = false, cellToPlace)
        val coordIsX = canBePlaced(horizontalPosition, boardCells)

        val shipLengthCoords =
          if (coordIsX) List.range(cellToPlace.x, cellToPlace.x + shipSize)
          else List.range(cellToPlace.y, cellToPlace.y + shipSize)

        val updatedBoardCells = shipLengthCoords.foldRight(boardCells)((coord, cells) => {
          cells.updated(if (coordIsX) Cell(coord, cellToPlace.y) else Cell(cellToPlace.x, coord), true)
        })

        if (coordIsX) (horizontalPosition, updatedBoardCells)
        else (verticalPosition, updatedBoardCells)
      })

    def calculateShipPositions(
      boardCells: TreeMap[Cell, Occupied],
      shipSizes: List[Int]
    ): Option[List[ShipPosition]] = {
      // solving this imperative way, because its much more performant here
      for (i <- shipSizes.indices) {
        val shipSize = shipSizes(i)
        lazy val otherShips = shipSizes.zipWithIndex.filterNot { case (_, idx) => i == idx }.map(_._1)
        val positionsWithNewShipAdded =
          tryGetPositionForShip(shipSize, boardCells)
          .flatMap { case (thisShipPos, leftOverEmptyCells) =>
            calculateShipPositions(leftOverEmptyCells, otherShips).map(thisShipPos :: _)
          }
          .fold(List.empty[ShipPosition])(newPositions => newPositions)

        if (positionsWithNewShipAdded.nonEmpty)
          return Some(positionsWithNewShipAdded)
      }

      if (shipSizes.isEmpty) Some(List.empty) else None
    }

    def generateShips(rows: Int, columns: Int, ships: List[Int]): Either[Option[Board], ErrorMsg] =
      generateShips(rows, columns, ships, java.lang.System.currentTimeMillis)

    // returns Some(Board) if all ships were places successfully on the board,
    // None - if ships cant fit on the board
    // ErrorMsg - if board is too big and StackOverflowException is thrown because algorithm is recursive
    def generateShips(rows: Int, columns: Int, ships: List[Int], seed: Long) : Either[Option[Board], ErrorMsg] = {
      try {
        val rnd = new Random(seed)
        val occupiedBoardCells: TreeMap[Cell, Occupied] = TreeMap(
          List.range(0, columns)
            .flatMap(x => List.range(0, rows).map(y => Cell(x, y) -> false)): _*
        )
        val board =
          calculateShipPositions(occupiedBoardCells, rnd.shuffle(ships))
          .map(shipPositions =>
            shipPositions.foldRight(Board.empty(rows, columns))((shipPos, board) =>
              board.placeShip(shipPos)._1
            )
          )

        Left(board)
      } catch { case e: Exception =>
        Right("An Error occurred while trying to generate Board: " + e)
      }
    }

    def shoot(board: Board, row: Int, column: Int): Board = {
      val cellToShoot = Cell(row, column)

      if (!board.containsCell(cellToShoot) || board.gameState == GameFinished)
        return board

      (board.healthyShips ++ board.damagedShips)
        .find(_.healthyCells.contains(cellToShoot))
        .flatMap(ship => {
          val (updatedHealthy, updatedDamaged) = (
            board.healthyShips.filterNot(_ == ship),
            board.damagedShips.filterNot(_ == ship)
          )
          ship.getHit(cellToShoot).map((_, updatedHealthy, updatedDamaged))
        })
        .map { case (shipThatGotHit, finalHealthy, updatedDamaged) =>
          val (finalDamaged, finalDestroyed) =
            if (shipThatGotHit.destroyed) (updatedDamaged, shipThatGotHit +: board.destroyedShips)
            else (shipThatGotHit +: updatedDamaged, board.destroyedShips)

          (finalHealthy, finalDamaged, finalDestroyed)
        }
        .fold(
          new Board(
            board.numOfRows,
            board.numOfCol,
            board.healthyShips,
            board.damagedShips,
            board.destroyedShips,
            cellToShoot :: board.missedCells,
            gameState = GameStarted
          )
        ) { case (updatedHealthy, updatedDamaged, updatedDestroyed) =>
          new Board(
            board.numOfRows,
            board.numOfCol,
            updatedHealthy,
            updatedDamaged,
            updatedDestroyed,
            board.missedCells,
            gameState =
              if (updatedHealthy.isEmpty && updatedDamaged.isEmpty)
                GameFinished else GameStarted
          )
        }
    }
  }
}
